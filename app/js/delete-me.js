'use strict';

$(document).ready(function () {
	var arrPage = [
		{ label: 'Главная', value: 'index' },
		{ label: 'Галерея. Альбомы', value: 'gallery' },
		{ label: 'Галерея. Альбом', value: 'gallery-album' },
		{ label: 'Галерея. Инстаграм', value: 'gallery-inst' },
		{ label: 'Видео-отзывы', value: 'reviews' },
		{ label: 'Фото-отзывы', value: 'reviews-2' },
		{ label: 'Новости', value: 'news' },
		{ label: 'Новость', value: 'news-post' },
		{ label: 'Статьи', value: 'articles' },
		{ label: 'Статья', value: 'articles-post' },
		{ label: 'О компании', value: 'about' },
		{ label: 'FAQ', value: 'faq' },
		{ label: 'Контакты', value: 'contacts' },
		{ label: '404', value: '404' },
		{ label: 'Каталог', value: 'catalog' },
		{ label: 'Избранное', value: 'favorites' },
		{ label: 'Продукт', value: 'product' },
		{ label: '-----', value: '#' },
		{ label: 'Модальные окна', value: 'popups' },
		// { label: 'test', value: 'test' },

		{ label: 'Вверх', value: '#' }
	];
	var textColor = "white",
	    bgColor = "#343434";

	var $list = $('<ol id="pages2342"></ol>')
	$list.appendTo('body').css({
		'position': 'fixed',
		'left': -210,
		'width': 220,
		'top': '50%',
		'max-height': '100%',
		'overflow': 'auto',
		'margin': 0,
		'padding': '20px',
		'border': '1px solid '+textColor,
		'border-left': 0,
		'background': bgColor,
		'zIndex': 54512, 'fontSize': 14,
		'color': textColor,
		'fontFamily': 'Arial, sans-serif',
		'lineHeight': '20px',
		'opacity': '0.6',
		'box-sizing': 'border-box',
	});

	for (var i = 0; i < arrPage.length; i++) {
		$list.append('<li><a ' + (arrPage[i].className? "class="+arrPage[i].className:'') + ' href="' + arrPage[i].value + '.html">' + arrPage[i].label + '</a></li>');
	}
	$('li', $list).css({
		'fontSize': 12,
		'color': textColor
	});

	$('a', $list).css({
		'display': 'inline-block',
		'width': '100%',
		'fontSize': 14,
		'color': textColor,
		'text-decoration': 'none'
	});

	$('li:last', $list).prepend('^').append('^').css({
		'fontWeight': 'bold',
		'listStyle': 'none',
		'textAlign': 'center'
	})
	.find('a')
	.attr('href', '#')
	.css({
		'width': 'auto'
	});

	$('<li><b id="arrow">&raquo;</b></li>').appendTo($list).css({
		'position': 'absolute',
		'top': '50%', 'right': 2,
		'height': 12,
		'margin-top': -12,
		'listStyle': 'none'
	});

	$list.css({
		marginTop: '-'+($list.outerHeight() / 2)+'px'
	})

	$('#arrow').css({ 'fontSize': 12, 'color': textColor });
	$list.hover(function () {
		$(this).css({ 'left': 0, 'opacity': '1' });
	}, function () {
		$(this).css({ 'left': -210, 'opacity': '0.6' });
	});
	$('a', $list).hover(function () {
		$(this).css('color', 'orange');
	}, function () {
		$(this).css('color', textColor);
	});
});