jQuery(document).ready(function ($) {
  function is_touch_device() {
    return !!('ontouchstart' in window);
  }

  var mainScroll = window.Scrollbar.init(document.querySelector('#smooth-scrollbar'), {
    damping: 0.05
  });
  var isMobile = {
    Android: function Android() {
      return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function BlackBerry() {
      return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function iOS() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function Opera() {
      return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function Windows() {
      return navigator.userAgent.match(/IEMobile/i);
    },
    any: function any() {
      return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
    }
  };
  mainScroll.addListener(function (status) {
    if (status.offset.y > 0) {
      $(".header").addClass("fixed");
    } else {
      $('.header').removeClass("fixed");
    }
  });
  $('img, a').on('dragstart', function (event) {
    event.preventDefault();
  });

  if ($("#main-map").length > 0) {
    var mainMap;
    ymaps.ready(function () {
      mainMap = new ymaps.Map("main-map", {
        center: [55.65465875, 37.55585454],
        zoom: 16,
        controls: []
      });
      mainMap.controls.add('zoomControl', {
        "float": 'none',
        position: {
          top: '8px',
          left: '8px'
        }
      });
      myGeoObject = new ymaps.GeoObject({});
      mainMap.geoObjects.add(myGeoObject).add(new ymaps.Placemark([55.65413707, 37.55587600], {
        hintContent: ""
      }, {
        iconLayout: 'default#image',
        iconImageHref: 'img/mark.svg',
        iconImageSize: [44, 59],
        iconImageOffset: [-22, -59]
      }));
      mainMap.cursors.push('none');
    });
  }

  if ($('#distributors-map').length) {
    var map, objectManager;
    ymaps.ready(function () {
      map = new ymaps.Map("distributors-map", {
        center: [55.65465875, 37.55585454],
        zoom: 3,
        controls: []
      });
      objectManager = new ymaps.ObjectManager({
        clusterize: false,
        gridSize: 32,
        clusterDisableClickZoom: true
      });
      map.controls.add('zoomControl', {
        "float": 'none',
        size: 'small',
        position: {
          top: '203px',
          right: '8px'
        }
      });
      map.controls.add('geolocationControl', {
        "float": 'none',
        position: {
          top: '269px',
          right: '8px'
        }
      });
      map.behaviors.disable('scrollZoom');
      map.geoObjects.add(objectManager); // Запрос на получение JSON для отрисовки меток на карте

      $.ajax({
        url: "js/data.json"
      }).done(function (data) {
        objectManager.add(data);
      });

      var filterPoints = function filterPoints(data) {
        return function (obj) {
          var mapInfo = obj.properties.hintContent.split(', ').map(function (info) {
            return info.toLowerCase();
          });
          var countryFromForm = data[0].value.toLowerCase();
          var cityFromForm = data[1].value.toLowerCase();

          if (countryFromForm && cityFromForm) {
            return countryFromForm === mapInfo[0] && cityFromForm === mapInfo[1];
          } else if (countryFromForm && !cityFromForm) {
            return countryFromForm === mapInfo[0];
          } else if (!countryFromForm && cityFromForm) {
            return cityFromForm === mapInfo[1];
          }

          return true;
        };
      };

      $('.js-filter-map').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serializeArray();
        objectManager.setFilter(filterPoints(data));
      });
      map.cursors.push('none');
    });
  }

  $('.js-more-distributors').on('click', function (e) {
    e.preventDefault(); // Запрос на получение дистрибьютеров

    $.get('/distributors').done(function (response) {
      var distributorsBlock = $('.js-distributors');
      distributorsBlock.css({
        height: 'auto'
      }).append(response);
    });
  });
  $('.form').submit(function () {
    var form = $(this);
    form.find('.error').removeClass('error'); //проверяем поля формы на пустоту

    form.find('.required').each(function () {
      if ($(this).val() == '' || $(this).attr('type') == 'tel' && $(this).val().indexOf('_') > -1) {
        $(this).addClass('error');
      }
    });
    form.find('.required-email').each(function () {
      var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

      if ($(this).val() == '' || !pattern.test($(this).val())) {
        $(this).addClass('error');
      }
    }); // если ошибок нет то отправляем данные

    return false;
  }); // Popups

  var magnificPopupArgs = {
    type: 'inline',
    removalDelay: 400,
    mainClass: 'mfp-fade',
    tClose: 'Закрыть'
  };

  function popup_open(src, type) {
    var args = $.extend(true, {}, magnificPopupArgs);
    if (type == 'iframe' || type == 'image') args['type'] = type;
    args['items'] = {
      src: src
    };
    $.magnificPopup.open(args);
  }

  $('body').on('click', '[data-popup]', function () {
    var type = $(this).attr('data-popup');
    popup_open($(this).attr('href'), type);
    return false;
  });
  $('.content-gallery, .js-gallery').each(function () {
    var items = $(this).find('a.main-img').not('[data-popup]'),
        args = $.extend(true, {}, magnificPopupArgs);
    args['type'] = 'image';
    args['gallery'] = {
      enabled: true
    };
    items.magnificPopup(args);
  }); // Popups END

  $('[type="tel"]').mask('+7 (999) 999-99-99');
  $('.mob-menu__menu>li>a').click(function () {
    var li = $(this).closest('li'),
        ul = $(this).siblings('ul');

    if (ul.length > 0) {
      li.toggleClass('dropdowned');
      ul.fadeToggle();
      return false;
    }
  });

  function mobMenuShow() {
    $('.header__toggle').addClass('active');
    $('.mob-menu').fadeIn(400);
    $('body').css({
      'overflow': 'hidden'
    });
    $('.header').addClass('menu-show');
  }

  function mobMenuClose() {
    $('.header__toggle').removeClass('active');
    $('.mob-menu').fadeOut(400);
    $('body').css({
      'overflow': ''
    });
    $('.header').removeClass('menu-show');
  }

  $(document).on('click', function (evt) {
    if ($('.header__toggle').hasClass('active') && !$(evt.target).closest('.header__toggle').length && !$(evt.target).closest('.mob-menu__wrap').length) {
      mobMenuClose();
    }
  });
  $('.header__toggle').click(function () {
    if ($(this).hasClass('active')) {
      mobMenuClose();
    } else {
      mobMenuShow();
    }
  }); // Sliders

  var firstSlider = new Swiper('.first-slider', {
    init: false,
    speed: 1800,
    spaceBetween: 100,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    }
  });

  if ($(firstSlider.params.el).length > 0) {
    var firstSides = function firstSides() {
      $('.first-slider .swiper-pagination, .first__mouse').width($('.first__wrap').offset().left);
    };

    firstSlider.on('init', function () {
      var dots = firstSlider.pagination.bullets.length;
      var dot = firstSlider.realIndex + 1;
      dots = dots < 10 ? '0' + dots : dots;
      dot = dot < 10 ? '0' + dot : dot;
      $('.first-slider').append('<div class="swiper-fraction"><span>' + dot + '</span><span>/ ' + dots + '</span></div>');
    }).init();
    firstSlider.on('slideChange', function () {
      var dot = firstSlider.realIndex + 1;
      dot = dot < 10 ? '0' + dot : dot;
      $('.first-slider .swiper-fraction span').eq(0).text(dot);
    });
    firstSides();
    $(window).on('load resize', function () {
      firstSides();
    });
  }

  var benefitsSlider = new Swiper('.benefits-slider', {
    slidesPerView: 3,
    speed: 1800,
    spaceBetween: 24,
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    },
    breakpoints: {
      1299: {
        slidesPerView: 'auto'
      },
      767: {
        centeredSlides: true,
        slidesPerView: 'auto'
      }
    }
  });
  var newsSlider = new Swiper('.news-slider', {
    slidesPerView: 1,
    speed: 1200,
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    }
  });
  $('.v-review__goto').each(function () {
    $(this).attr('data-index', $(this).closest('.v-review').index());
  });
  var vReviewsTextSlider = new Swiper('.v-reviews__text-box', {
    inint: false,
    speed: 1800,
    slidesPerView: 1,
    effect: 'fade',
    allowTouchMove: false,
    followFinger: false,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    fadeEffect: {
      crossFade: true
    }
  });
  var vReviewsSlider = new Swiper('.v-reviews-slider', {
    init: false,
    speed: 1800,
    slidesPerView: 1,
    thumbs: {
      swiper: vReviewsTextSlider
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    },
    breakpoints: {
      767: {
        spaceBetween: 24
      }
    }
  });

  if ($(vReviewsSlider.params.el).length > 0) {
    if ($(vReviewsTextSlider.params.el).length) {
      vReviewsTextSlider.on('init', function () {
        vReviewsSlider.thumbs.swiper = vReviewsTextSlider;
      }).init();
    }

    vReviewsSlider.on('init', function () {
      var dots = vReviewsSlider.pagination.bullets.length;
      var dot = vReviewsSlider.realIndex + 1;
      dots = dots < 10 ? '0' + dots : dots;
      dot = dot < 10 ? '0' + dot : dot;
      $('.v-reviews-fraction').html('<span>' + dot + '</span><span>/ ' + dots + '</span>');
    }).init();
    vReviewsSlider.on('slideChange', function () {
      var dot = vReviewsSlider.realIndex + 1;
      dot = dot < 10 ? '0' + dot : dot;
      $('.v-reviews-fraction span').eq(0).text(dot);
    });
    $('body').on('click', '.v-review__goto', function () {
      vReviewsSlider.slideTo($(this).attr('data-index'));
    });
  }

  var albumsSlider = new Swiper('.albums-slider', {
    slidesPerView: 'auto',
    speed: 1800,
    spaceBetween: 24,
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    }
  });
  var contactsSlider = new Swiper('.contacts-slider', {
    slidesPerView: 1,
    effect: 'fade',
    speed: 1800,
    noSwiping: true,
    allowTouchMove: false,
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    }
  });
  var productSlider = new Swiper('.product-slider .swiper-container', {
    effect: 'fade',
    speed: 1200,
    fadeEffect: {
      crossFade: true
    },
    spaceBetween: 100,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    }
  });

  if ($(productSlider.params.el).length > 0) {
    var productSides = function productSides() {
      $('.product-slider .swiper-pagination').width($('.product__wrapper').offset().left);
    };

    productSlider.on('slideChange', function () {
      $('.product-action__thumb').removeClass('active').eq(productSlider.realIndex).addClass('active');
    });
    $('.product-action__thumb').click(function () {
      productSlider.slideTo($(this).index());
    }).eq(0).addClass('active');
    productSides();
    $(window).on('load resize', function () {
      productSides();
    });
  }

  var productAddonsSlider = new Swiper('.product-addons__slider .swiper-container', {
    spaceBetween: 100,
    speed: 1800,
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    }
  });

  if ($(productAddonsSlider.params.el).length > 0) {
    productAddonsSlider.on('slideChange', function () {
      $('.product-addons__thumb').removeClass('active').eq(productAddonsSlider.realIndex).addClass('active');
      $('.product-addons__item').hide().eq(productAddonsSlider.realIndex).fadeIn(250);
    });
    $('.product-addons__thumb').click(function () {
      productAddonsSlider.slideTo($(this).index());
    }).eq(0).addClass('active');
  }

  var articlesSlider = new Swiper('.articles-slider', {
    slidesPerView: 3,
    spaceBetween: 24,
    speed: 1800,
    navigation: {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next'
    },
    breakpoints: {
      1299: {
        slidesPerView: 'auto'
      }
    }
  }); // Sliders END

  $('.js-head').each(function () {
    var item = $(this),
        show = false;
    item.append('<span class="js-head__line"></span>');

    function itemFunc() {
      if (item.offset().top < $(window).height()) {
        item.addClass('js-head_start');
        setTimeout(function () {
          item.addClass('js-head_end');
        }, 500);
        show = true;
      }
    }

    $(window).load(function () {
      itemFunc();
    });

    if (!show) {
      mainScroll.addListener(function (status) {
        itemFunc();
      });
    }
  });
  $('.main-img').each(function () {
    var item = $(this),
        show = false;

    function itemFunc() {
      if (item.offset().top < $(window).height()) {
        item.addClass('main-img_show');
        setTimeout(function () {
          item.addClass('main-img_showed');
        }, 1000);
        show = true;
      }
    }

    $(window).load(function () {
      itemFunc();
    });

    if (!show) {
      mainScroll.addListener(function (status) {
        itemFunc();
      });
    }
  });
  $('.main-img__parallax').each(function () {
    var item = $(this).closest('.main-img'),
        item_top = item.offset().top,
        item_parallax = $(this);
    mainScroll.addListener(function (status) {
      var scrollTop = status.offset.y;
      top_border = item_parallax.outerHeight() - item.outerHeight();
      transform = scrollTop - item_top + $(window).height();
      transform = transform > 0 ? (transform / 12).toFixed(3) : 0;
      transform = top_border - transform > 0 ? transform : top_border;
      item_parallax.css({
        '-webkit-transform': 'translateY(' + transform + 'px)',
        'transform': 'translateY(' + transform + 'px)'
      });
    });
  });
  $('.gallery__grid').masonry({
    columnWidth: '.gallery-sizer',
    itemSelector: '.gallery-item',
    percentPosition: true
  });
  $('.reviews-inn__grid').masonry({
    columnWidth: '.reviews-inn__col',
    itemSelector: '.reviews-inn__col',
    percentPosition: true
  });
  $('.review__main').click(function () {
    if (is_touch_device()) {
      item_hover($(this).closest('.review'));
    }
  });
  $('.review').hover(function () {
    if (!is_touch_device()) {
      $(this).toggleClass('hover');
    }
  });
  $('.article').click(function () {
    if (is_touch_device()) {
      item_hover($(this).closest('.article'));
    }
  }).hover(function () {
    if (!is_touch_device()) {
      $(this).toggleClass('hover');
    }
  });

  function item_hover(item) {
    if (item.hasClass('hover')) {
      item.removeClass('hover');
    } else {
      item.addClass('hover');
    }

    $(document).on("click", function (e) {
      if (!item.is(e.target) && item.has(e.target).length === 0) {
        item.removeClass('hover');
      }
    });
  }

  function productInfoHover(i) {
    $('.product-info__list li').eq(i).addClass('active');
    $('.js-product-info-first li').eq(i).addClass('active');
    $('.js-product-info-second li').eq(i).addClass('active');
  }

  function productInfoReset() {
    $('.product-info__list li').removeClass('active');
    $('.js-product-info-first li').removeClass('active');
    $('.js-product-info-second li').removeClass('active');
  }

  if (!is_touch_device()) {
    $('.product-info__list li, .js-product-info-first li, .js-product-info-second li').hover(function () {
      productInfoHover($(this).index());
    }, function () {
      productInfoReset();
    });
  } else {
    $('.product-info__list li, .js-product-info-first li, .js-product-info-second li').click(function () {
      if (!$(this).hasClass('active')) {
        productInfoReset();
        productInfoHover($(this).index());
      }
    });
  }

  $('.faq-item__button').click(function () {
    $(this).toggleClass('active');
    $(this).siblings('.faq-item__hidden').slideToggle();
  });
  $('select').each(function () {
    var select = $(this),
        select_class = $(this).data('class') ? $(this).data('class') : '',
        placeholder = $(this).data('placeholder');
    select.wrap('<div class="select-wrapper ' + select_class + '"></div>');
    select.css('width', '100%').select2({
      placeholder: placeholder,
      minimumResultsForSearch: Infinity,
      dropdownParent: select.closest('.select-wrapper')
    });
  });
  $(".js-range-slider").ionRangeSlider({
    skin: "round",
    hide_min_max: true,
    force_edges: true
  });
  $('.filter-toggle').click(function () {
    $(this).toggleClass('active');
    $('.filter').slideToggle();
  });
  $(window).load(function () {
    if ($('#js-first-video').length > 0) {
      $('#js-first-video').html('<video loop="loop" autoplay="autoplay" muted="muted"><source src="video/sup.mp4" type="video/mp4"><source src="video/sup.mp4" type="video/webm"></video>');
    }
  });

  if ($('.js-cursor').length && $('.js-cursor-2').length) {
    var cursor = document.querySelector('.js-cursor'),
        cursor2 = document.querySelector('.js-cursor-2'),
        videoCursor = null,
        videoBlock = null;

    if (isMobile.any()) {
      cursor.setAttribute('style', 'display: none;');
      cursor2.setAttribute('style', 'display: none;');
    } else {
      document.addEventListener('mousemove', function (e) {
        if ($(e.target).closest('.js-hover').length) {
          cursor.setAttribute('style', 'transform: scale(2); left: ' + (e.pageX - cursor.offsetWidth / 2) + 'px; top: ' + (e.pageY - cursor.offsetWidth / 2) + 'px;');
          cursor2.setAttribute('style', 'transform: scale(1.5); left: ' + (e.pageX - cursor2.offsetWidth / 2) + 'px; top: ' + (e.pageY - cursor2.offsetWidth / 2) + 'px;');
        } else if ($(e.target).closest('.js-video').length) {
          videoBlock = $(e.target).closest('.js-video');
          videoCursor = videoBlock.siblings('.js-video-cursor');
          cursor.setAttribute('style', 'transform: scale(0); left: ' + (e.pageX - cursor.offsetWidth / 2) + 'px; top: ' + (e.pageY - cursor.offsetWidth / 2) + 'px;');
          cursor2.setAttribute('style', 'transform: scale(0); left: ' + (e.pageX - cursor2.offsetWidth / 2) + 'px; top: ' + (e.pageY - cursor2.offsetWidth / 2) + 'px;');
          videoCursor.css({
            transform: 'translate(' + (e.pageX - videoBlock.offset().left - videoCursor.innerWidth() / 2) + 'px, ' + (e.pageY - videoBlock.offset().top - videoCursor.innerHeight() / 2) + 'px)'
          });
        } else {
          cursor.setAttribute('style', 'left: ' + (e.pageX - cursor.offsetWidth / 2) + 'px; top: ' + (e.pageY - cursor.offsetWidth / 2) + 'px;');
          cursor2.setAttribute('style', 'left: ' + (e.pageX - cursor2.offsetWidth / 2) + 'px; top: ' + (e.pageY - cursor2.offsetWidth / 2) + 'px;');

          if (videoCursor) {
            videoCursor.removeAttr('style');
          }
        }
      });
    }
  }
});